from __future__ import print_function

import os.path
import json

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google.cloud import storage

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

def create_season(name_file):
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.json'):
        creds = Credentials.from_authorized_user_file('token.json', SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                name_file, SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.json', 'w') as token:
            token.write(creds.to_json())
    return creds

def propierties_spreedsheet(title, files):

    result = {}
    result['properties'] = {
            'title': title
        }
    sheets_tmp = []
    for sheet in files:
        sheets_tmp.append({'properties': {
            'title' : sheet
            }
            })
    result['sheets'] = sheets_tmp
    return result


def create_spreedsheet(title, sheets, file_credentials):
    try:
        service = build('sheets', 'v4', credentials=create_season(file_credentials))
        spreadsheet_body = propierties_spreedsheet(title, sheets)
        spreadsheet = service.spreadsheets().create(body=spreadsheet_body,
                                                    fields='spreadsheetId') \
            .execute()
        print(f"Spreadsheet ID: {(spreadsheet.get('spreadsheetId'))}")
        return spreadsheet.get('spreadsheetId')
    except HttpError as error:
        print(f"An error occurred: {error}")
        return error

def write_spreedsheet(name_sheet, spreadsheet_id, bucket, file_credentials, file_service_account):
    worksheet_name = name_sheet + '!'
    cell_range_insert = 'A1'
    values = values_json(name_sheet, bucket, file_service_account)
    value_range_body = {
        'majorDimension': 'ROWS',
        'values': values
    }
    try:
        service = build('sheets', 'v4', credentials=create_season(file_credentials))
        service.spreadsheets().values().update(
            spreadsheetId=spreadsheet_id,
            valueInputOption='USER_ENTERED',
            range=worksheet_name + cell_range_insert,
            body=value_range_body
        ).execute()
    except HttpError as error:
        print(f"An error occurred: {error}")
        return error
    pass

def values_json(name_file, name_bucket, file_service_account):

    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = file_service_account

    storage_client = storage.Client()
    bucket = storage_client.get_bucket(name_bucket)
    blob = bucket.get_blob(name_file)
    bt = blob.download_as_string()
    data = json.loads(bt.decode('utf-8'))

    fields_tmp = []
    for item in data:
        fields_tmp.append(tuple(item.keys()))
    fields =[]
    for lists in fields_tmp:
        for field in lists:
            if field not in fields:
                fields.append(field)

    values = []
    for register in data:
        subvalues = []
        for field in fields:
            try:
                subvalues.append(str(register[field]))
            except:
                subvalues.append("")
        values.append(tuple(subvalues))

    values.insert(0, tuple(fields))
    return values

if __name__ == '__main__':
    # Pass: title
    name_file_oath = "credencials"
    name_file_service_account = "Key_CloudStorage.json"
    name_spredsheets = "Hunty Test 1"
    bucket_name = "hunty_test_evalero"
    sheets = ["office_modality.json", "tamano_empresas.json"]
    id = create_spreedsheet(name_spredsheets, sheets, name_file_oath)
    for sheet in sheets:
        write_spreedsheet(sheet, id, bucket_name, name_file_oath, name_file_service_account)