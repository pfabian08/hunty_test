# hunty_test



## Getting started

# Bloque 1

Se desarrollo con interprete Python 3.9.13 o en su defecto el ultimo desplegado por la distribucion de [Anaconda](https://www.anaconda.com/products/distribution)

## Variables

- nme_file_oath el cual es el nombre del archivo sin la extension (.json) que se genera al crear la credencial OAth.
- name_file_service_account es el nombre del archivo con extension que se genera al cerar la cerdencia de Cuentas de Servicio.
- bucket_name nombre del bucket en Colud Storage.
- sheets nombre de los archivos a tomar para escribir e igualmente deja este valor para el nombre de las hojas.
- name_spredsheets nombre del archivo de Google Sheet.

### Nota: 
- Todas las hojas se creen en el mismo archivo de Google Sheet, se diseño para tomar las X cantidad de archivos (.json) que se llegaren a enviar en las variables sheets.
- Para los archivos leidos (.json) en caso de agregarse mas campos, no haber la misma cantidad de campos entre registros o estar en desorden, el script toma estas correcciones y deja los datos uniformemente.
- Los nombres de los archivos no pueden tener caracteres No ASCII.
- Las credenciales IAM (Cuentas de Servcio y ID Clientes OAuth 2.0) deben tener Adminstrador de Objetos de Storage y propietario.
- Los archivos/objetos leidos no deben estar en subcarpetas deben estar alojados directamente en la raiz del bucket.
- Tener espacio disponible en Google Drive para la creacion del archivo.

